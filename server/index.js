const express = require('express')
const consola = require('consola')
const morgan = require('morgan')
const { Nuxt, Builder } = require('nuxt')
const app = express()

// Import and Set Nuxt.js options
const config = require('../nuxt.config.js')
config.dev = !(process.env.NODE_ENV === 'production')

async function start() {
  // Init Nuxt.js
  const nuxt = new Nuxt(config)

  const { host, port } = nuxt.options.server

  // Build only in dev mode
  if (config.dev) {
    const builder = new Builder(nuxt)
    await builder.build()
  } else {
    await nuxt.ready()
  }

  morgan.token('id', function getId(req) {
    return req.id
  });

  var loggerFormat = ':id [:date[web]] ":method :url" :status :response-time';

  app.use(morgan(loggerFormat, {
      skip: function (req, res) {
          return res.statusCode < 400
      },
      stream: process.stderr
  }));

  app.use(morgan(loggerFormat, {
      skip: function (req, res) {
          return res.statusCode >= 400
      },
      stream: process.stdout
  }));


  // Give nuxt middleware to express
  app.use(nuxt.render)

  // Listen the server
  app.listen(port, host)
  consola.ready({
    message: `Server listening on http://${host}:${port}`,
    badge: true
  })
}
start()
