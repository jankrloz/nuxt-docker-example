FROM node:8.15.1-alpine

ENV HOST 0.0.0.0
ENV NODE_ENV production

RUN mkdir /app

COPY yarn.lock /app
COPY package.json /app

WORKDIR /app
RUN yarn install

COPY . /app

EXPOSE 3000

RUN yarn build
CMD [ "yarn", "start"]
